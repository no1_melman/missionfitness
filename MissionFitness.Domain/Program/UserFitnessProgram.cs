﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The user fitness program.
    /// Joins a fitness program to user.
    /// </summary>
    public class UserFitnessProgram : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the fitness program id.
        /// </summary>
        public int FitnessProgramId { get; set; }
    }
}
