﻿namespace MissionFitness.Gui.Models.FitnessAction
{
    /// <summary>
    /// The fitness action DTO.
    /// </summary>
    public class FitnessActionDto : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}