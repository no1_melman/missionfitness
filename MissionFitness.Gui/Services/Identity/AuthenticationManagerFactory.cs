﻿namespace MissionFitness.Gui.Services.Identity
{
    using System.Web;

    using Microsoft.Owin.Security;

    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The authentication manager factory.
    /// </summary>
    public class AuthenticationManagerFactory : IAuthenticationManagerFactory
    {
        /// <summary>
        /// Gets the Authentication Manager from the HTTP Context.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthenticationManager"/>.
        /// </returns>
        public IAuthenticationManager Create()
        {
            return HttpContext.Current.GetOwinContext().Authentication;
        }
    }
}