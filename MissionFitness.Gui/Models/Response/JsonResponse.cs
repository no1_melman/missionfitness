﻿namespace MissionFitness.Gui.Models.Response
{
    using System.Collections.Generic;

    /// <summary>
    /// The json response.
    /// </summary>
    /// <typeparam name="T">
    /// Objects that are being returned
    /// </typeparam>
    public class JsonResponse<T>
        where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JsonResponse{T}"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        public JsonResponse(string message, bool success)
        {
            this.Message = message;
            this.Success = success;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonResponse{T}"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        /// <param name="errors">
        /// The errors.
        /// </param>
        public JsonResponse(string message, bool success, List<string> errors)
            : this(message, success)
        {
            this.Errors = errors;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonResponse{T}"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        /// <param name="dataList">
        /// The data list.
        /// </param>
        public JsonResponse(string message, bool success, List<T> dataList)
            : this(message, success, new List<string>())
        {
            this.DataList = dataList;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="JsonResponse{T}"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="success">
        /// The success.
        /// </param>
        /// <param name="data">
        /// The data.
        /// </param>
        public JsonResponse(string message, bool success, T data)
            : this(message, success, new List<string>())
        {
            this.Data = data;
            this.DataList = new List<T>();
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether success.
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public List<T> DataList { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Gets or sets the errors.
        /// </summary>
        public List<string> Errors { get; set; }
    }
}