namespace MissionFitness.Domain.Migrations
{
    using System.Data.Entity.Migrations;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using MissionFitness.Domain.Identity;

    /// <summary>
    /// The configuration.
    /// </summary>
    internal sealed class Configuration : DbMigrationsConfiguration<MissionContext>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Configuration"/> class.
        /// </summary>
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
        }

        /// <summary>
        /// The seed.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        protected override void Seed(MissionContext context)
        {
            var userManager =
                new UserManager<User, int>(new UserStore<User, Role, int, UserLogin, UserRole, UserClaim>(context));
            var roleManager = new RoleManager<Role, int>(new RoleStore<Role, int, UserRole>(context));

            var user = new User
                           {
                               FirstName = "Callum",
                               LastName = "Linington",
                               UserName = "cliningt",
                               Email = "callum.linington@live.co.uk"
                           };
            userManager.Create(
                user,
                "easyPassword");

            context.SaveChanges();

            var adminExists = roleManager.RoleExists("Administrator");
            if (!adminExists)
            {
                roleManager.Create(new Role { Name = "Administrator" });
            }

            var memberExists = roleManager.RoleExists("Member");
            if (!memberExists)
            {
                roleManager.Create(new Role { Name = "Member" });
            }

            var guestExists = roleManager.RoleExists("Guest");
            if (!guestExists)
            {
                roleManager.Create(new Role { Name = "Guest" });
            }

            context.SaveChanges();

            userManager.AddToRole(user.Id, "Administrator");

            context.SaveChanges();
        }
    }
}
