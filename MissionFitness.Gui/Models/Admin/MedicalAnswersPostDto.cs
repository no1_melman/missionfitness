﻿namespace MissionFitness.Gui.Models.Admin
{
    using System.Collections.Generic;

    /// <summary>
    /// The medical answers post dto.
    /// </summary>
    public class MedicalAnswersPostDto
    {
        /// <summary>
        /// Gets or sets the medical profile id.
        /// </summary>
        public int MedicalProfileId { get; set; }

        /// <summary>
        /// Gets or sets the medical answer dtos.
        /// </summary>
        public List<MedicalAnswerDto> MedicalAnswerDtos { get; set; }
    }
}