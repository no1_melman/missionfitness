﻿namespace MissionFitness.Gui.Models.Admin
{
    using System;
    using System.Data.Entity;
    using System.Threading.Tasks;

    using AutoMapper;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Domain.Medical;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The medical question service.
    /// </summary>
    public class MedicalQuestionService : IMedicalQuestionService<MedicalQuestionDto>
    {
        /// <summary>
        /// The context factory.
        /// </summary>
        private readonly IMissionContextFactory contextFactory;

        /// <summary>
        /// The mapping engine.
        /// </summary>
        private readonly IMappingEngine mappingEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="MedicalQuestionService"/> class.
        /// </summary>
        /// <param name="contextFactory">
        /// The context factory.
        /// </param>
        /// <param name="mappingEngine">
        /// The mapping engine.
        /// </param>
        public MedicalQuestionService(
            IMissionContextFactory contextFactory,
            IMappingEngine mappingEngine)
        {
            this.contextFactory = contextFactory;
            this.mappingEngine = mappingEngine;
        }

        /// <summary>
        /// The create medical question.
        /// </summary>
        /// <param name="medicalQuestionDto">
        /// The medical question DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task CreateMedicalQuestion(MedicalQuestionDto medicalQuestionDto)
        {
            using (var context = this.contextFactory.Create())
            {
                var medicalQuestion =
                    await context.MedicalQuestions.FirstOrDefaultAsync(q => q.Id == medicalQuestionDto.Id)
                    ?? new MedicalQuestion();

                this.mappingEngine.Map(medicalQuestionDto, medicalQuestion);

                if (medicalQuestion.Id == 0)
                {
                    context.MedicalQuestions.Add(medicalQuestion);
                }

                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// The delete medical question.
        /// </summary>
        /// <param name="medicalQuestionId">
        /// The medical question id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// Medical Question doesn't exist
        /// </exception>
        public async Task DeleteMedicalQuestion(int medicalQuestionId)
        {
            using (var context = this.contextFactory.Create())
            {
                var medicalQuestion = await context.MedicalQuestions.FirstOrDefaultAsync(q => q.Id == medicalQuestionId);

                if (medicalQuestion != null)
                {
                    context.MedicalQuestions.Remove(medicalQuestion);

                    await context.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("Medical Question doesn't exist");
                }
            }
        }
    }
}