﻿namespace MissionFitness.Gui.Controllers.Api.Admin
{
    using System.Threading.Tasks;
    using System.Web.Http;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Gui.Models.Admin;
    using MissionFitness.Gui.Models.Response;

    /// <summary>
    /// The medical profile controller.
    /// </summary>
    [Authorize(Roles = "Administrator"),
    RoutePrefix("Api/MedicalProfile")]
    public class MedicalProfileController : ServiceApiController
    {
        /// <summary>
        /// The medical profile service.
        /// </summary>
        private readonly IMedicalProfileService<MedicalQuestionDto, MedicalAnswerDto> medicalProfileService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MedicalProfileController"/> class.
        /// </summary>
        /// <param name="medicalProfileService">
        /// The medical profile service.
        /// </param>
        public MedicalProfileController(
            IMedicalProfileService<MedicalQuestionDto, MedicalAnswerDto> medicalProfileService)
        {
            this.medicalProfileService = medicalProfileService;
            this.medicalProfileService.User = this.User;
        }

        /// <summary>
        /// The get questions async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        [Route("Questions")]
        public async Task<JsonResponse<MedicalQuestionDto>> GetQuestionsAsync()
        {
            return await this.ExecuteQuery(this.medicalProfileService.GetQuestionsAsync());
        }

        /// <summary>
        /// The post medical answers async (upsert medical answers).
        /// </summary>
        /// <param name="medicalAnswersPostDto">
        /// The medical answers post DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<JsonResponse<object>> PostAsync(MedicalAnswersPostDto medicalAnswersPostDto)
        {
            return
                await
                this.ExecuteService(
                    this.medicalProfileService.SaveAnswersAsync(
                        medicalAnswersPostDto.MedicalProfileId,
                        medicalAnswersPostDto.MedicalAnswerDtos));
        }

        /// <summary>
        /// The get medical profile async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<JsonResponse<MedicalAnswerDto>> GetAsync()
        {
            return await this.ExecuteQuery(this.medicalProfileService.GetAnswersAsync());
        }
    }
}
