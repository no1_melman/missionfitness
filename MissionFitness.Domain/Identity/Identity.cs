﻿namespace MissionFitness.Domain.Identity
{
    using Microsoft.AspNet.Identity.EntityFramework;

    /// <summary>
    /// The user.
    /// </summary>
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        public string LastName { get; set; }
    }

    /// <summary>
    /// The user login.
    /// </summary>
    public class UserLogin : IdentityUserLogin<int>
    {
    }

    /// <summary>
    /// The user claim.
    /// </summary>
    public class UserClaim : IdentityUserClaim<int>
    {
    }

    /// <summary>
    /// The user role.
    /// </summary>
    public class UserRole : IdentityUserRole<int>
    {
    }

    /// <summary>
    /// The role.
    /// </summary>
    public class Role : IdentityRole<int, UserRole>
    {
    }
}
