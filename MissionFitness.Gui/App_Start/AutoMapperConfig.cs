﻿namespace MissionFitness.Gui.App_Start
{
    using AutoMapper;

    using MissionFitness.Gui.MappingProfiles.Admin;
    using MissionFitness.Gui.MappingProfiles.FitnessAction;

    /// <summary>
    /// The auto mapper config.
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// The configure.
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(ConfigureProfiles);

            // Mapper.AssertConfigurationIsValid();
        }

        /// <summary>
        /// The configure profiles.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        private static void ConfigureProfiles(IConfiguration configuration)
        {
            configuration.AddProfile(new FitnessActionProfile());
            configuration.AddProfile(new MedicalProfileProfile());
            configuration.AddProfile(new MemberProfile());
        }
    }
}