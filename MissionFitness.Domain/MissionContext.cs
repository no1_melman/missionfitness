﻿namespace MissionFitness.Domain
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity.EntityFramework;

    using MissionFitness.Domain.Identity;
    using MissionFitness.Domain.Medical;
    using MissionFitness.Domain.Program;
    using MissionFitness.Domain.Register;

    /// <summary>
    /// The mission context.
    /// </summary>
    public class MissionContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        /// <summary>
        /// Gets or sets the fitness actions.
        /// </summary>
        public DbSet<FitnessAction> FitnessActions { get; set; }

        /// <summary>
        /// Gets or sets the weight set reps.
        /// </summary>
        public DbSet<WeightSetRep> WeightSetReps { get; set; }

        /// <summary>
        /// Gets or sets the user fitness programs.
        /// </summary>
        public DbSet<UserFitnessProgram> UserFitnessPrograms { get; set; }

        /// <summary>
        /// Gets or sets the fitness programs.
        /// </summary>
        public DbSet<FitnessProgram> FitnessPrograms { get; set; }

        /// <summary>
        /// Gets or sets the fitness program actions.
        /// </summary>
        public DbSet<FitnessProgramActions> FitnessProgramActionses { get; set; }

        /// <summary>
        /// Gets or sets the fitness program actions WSRs.
        /// </summary>
        public DbSet<FitnessProgramActionsWsr> FitnessProgramActionsWsrs { get; set; }

        /// <summary>
        /// Gets or sets the user registrations.
        /// </summary>
        public DbSet<UserRegister> UserRegistrations { get; set; }

        /// <summary>
        /// Gets or sets the medical questions.
        /// </summary>
        public DbSet<MedicalQuestion> MedicalQuestions { get; set; }

        /// <summary>
        /// Gets or sets the medical answers.
        /// </summary>
        public DbSet<MedicalAnswer> MedicalAnswers { get; set; }
    }
}
