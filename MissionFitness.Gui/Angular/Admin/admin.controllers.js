﻿(function() {
    angular.module('admin.module')
        .controller('admin.controllers.memberRegistration', ['admin.services.memberService', memberRegistrationController])
        .controller('admin.controllers.medicalProfile', ['admin.services.medicalProfile', 'logger', medicalProfileController]);

    function memberRegistrationController(memberService) {
        var vm = this;

        vm.member = {};

        vm.submit = function() {
            memberService.registerMember(vm.member).then(function(data) {
                if (data.success) {
                    // do something successful
                } else {
                    // show the fail!
                }
            });
        }
    }
    function medicalProfileController(medicalProfileService, logger) {
        var vm = this;

        // bindable properties
        vm.answers = [];
        vm.questions = [];
        vm.grouped = [];

        // requests
        medicalProfileService.get().then(getAnswersThen);
        medicalProfileService.getQuestions().then(getQuestionsThen);

        function getAnswersThen(data) {
            if (data.success) {
                vm.answers = data.dataList;
                done.answers = true;
                group();
            } else {
                logger.error('Unable to get answers', data, 'Error Retrieving Answers');
            }
        }

        function getQuestionsThen(data) {
            if (data.success) {
                vm.questions = data.dataList;
                done.questions = true;
                group();
            } else {
                logger.error('Unable to get questions', data, 'Error Retrieving Questions');
            }
        }

        var done = { answers: false, questions: false };
        function group() {
            if (done.answers == false || done.questions == false) {
                return;
            }

            vm.grouped = [];
            angular.forEach(vm.questions, function (q) {

                var answerFound = false;
                angular.forEach(vm.answers, function (a) {
                    if (q.id == a.medicalQuestion.id) {
                        answerFound = true;
                        vm.grouped.push({
                            question: q,
                            answer: a
                        });
                    }
                });

                if (answerFound == false) {
                    vm.grouped.push({
                        question: q,
                        answer: { answer: false }
                    });
                }
            });
        }
    }
})();