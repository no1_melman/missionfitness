﻿namespace MissionFitness.Gui.App_Start.Bundles.BundlePartials
{
    using MissionFitness.Common.Bundling;
    using MissionFitness.Gui.App_Start.Bundles.Formatters;

    /// <summary>
    /// The security bundles.
    /// </summary>
    public static class SecurityBundles
    {
        /// <summary>
        /// The scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial Scripts()
        {
            return new BundlePartial("Security")
                .UseFormatter(new StandardPathFormatter())
                .AsModule()
                .IncludeServices()
                .IncludeDirectives()
                .IncludeControllers();
        }
    }
}