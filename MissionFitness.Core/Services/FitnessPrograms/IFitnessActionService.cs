﻿namespace MissionFitness.Core.Services.FitnessPrograms
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    /// The Fitness Action Service interface.
    /// </summary>
    /// <typeparam name="TFitnessActionDto">
    /// Fitness Action DTO
    /// </typeparam>
    public interface IFitnessActionService<TFitnessActionDto>
        where TFitnessActionDto : class
    {
        /// <summary>
        /// The save fitness action async.
        /// </summary>
        /// <param name="fitnessActionDto">
        /// The fitness action DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task SaveFitnessActionAsync(TFitnessActionDto fitnessActionDto);

        /// <summary>
        /// The delete fitness action async.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task DeleteFitnessActionAsync(int id);

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TFitnessActionDto>> GetAllAsync();

        /// <summary>
        /// The get by fitness program.
        /// </summary>
        /// <param name="fitnessProgramId">
        /// The fitness program id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TFitnessActionDto>> GetByFitnessProgram(int fitnessProgramId);
    }
}
