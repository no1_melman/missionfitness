﻿namespace MissionFitness.Gui.Services.Admin
{
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using Microsoft.AspNet.Identity;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Domain.Medical;
    using MissionFitness.Gui.Models.Admin;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The medical profile service.
    /// </summary>
    public class MedicalProfileService : IMedicalProfileService<MedicalQuestionDto, MedicalAnswerDto>
    {
        /// <summary>
        /// The context factory.
        /// </summary>
        private readonly IMissionContextFactory contextFactory;

        /// <summary>
        /// The mapping engine.
        /// </summary>
        private readonly IMappingEngine mappingEngine;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="MedicalProfileService"/> class.
        /// </summary>
        /// <param name="contextFactory">
        /// The context factory.
        /// </param>
        /// <param name="mappingEngine">
        /// The mapping engine.
        /// </param>
        public MedicalProfileService(
            IMissionContextFactory contextFactory,
            IMappingEngine mappingEngine)
        {
            this.contextFactory = contextFactory;
            this.mappingEngine = mappingEngine;
        }

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        public IPrincipal User { get; set; }

        /// <summary>
        /// The get questions async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<MedicalQuestionDto>> GetQuestionsAsync()
        {
            using (var context = this.contextFactory.Create())
            {
                return await context.MedicalQuestions.Project().To<MedicalQuestionDto>().ToListAsync();
            }
        }

        /// <summary>
        /// The get answers async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<MedicalAnswerDto>> GetAnswersAsync()
        {
            using (var context = this.contextFactory.Create())
            {
                var userId = this.User.Identity.GetUserId<int>();
                var answers = await
                    context.MedicalAnswers.Where(m => m.UserId == userId)
                        .Join(
                            context.MedicalQuestions,
                            a => a.MedicalQuestionId,
                            q => q.Id,
                            (answer, question) => new { answer, question }).ToListAsync();

                var dtod = answers.Select(
                    x =>
                        {
                            var answerDto = this.mappingEngine.Map<MedicalAnswerDto>(x.answer);
                            var questionDto = this.mappingEngine.Map<MedicalQuestionDto>(x.question);

                            answerDto.MedicalQuestion = questionDto;

                            return answerDto;
                        });

                return dtod.ToList();
            }
        }

        /// <summary>
        /// The save answers async.
        /// </summary>
        /// <param name="medicalProfileId">
        /// The medical profile id.
        /// </param>
        /// <param name="medicalAnswerDtos">
        /// The medical answer DTOs.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task SaveAnswersAsync(int medicalProfileId, List<MedicalAnswerDto> medicalAnswerDtos)
        {
            using (var context = this.contextFactory.Create())
            {
                var userId = this.User.Identity.GetUserId<int>();
                var existingAnswers = await context.MedicalAnswers.Where(m => m.UserId == userId).ToListAsync();

                if (existingAnswers.Any())
                {
                    context.MedicalAnswers.RemoveRange(existingAnswers);
                }

                var answers = medicalAnswerDtos.Select(this.mappingEngine.Map<MedicalAnswer>);
                
                foreach (var m in answers)
                {
                    m.UserId = userId;
                }

                context.MedicalAnswers.AddRange(answers);

                await context.SaveChangesAsync();
            }
        }
    }
}