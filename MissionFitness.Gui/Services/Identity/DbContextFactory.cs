﻿namespace MissionFitness.Gui.Services.Identity
{
    using MissionFitness.Domain;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The database context factory.
    /// </summary>
    public class DbContextFactory : IMissionContextFactory
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="MissionContext"/>.
        /// </returns>
        public MissionContext Create()
        {
            return new MissionContext();
        }
    }
}