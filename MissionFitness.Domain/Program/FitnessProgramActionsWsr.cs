﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The fitness program actions weight/sets/reps.
    /// Joins the fitness program actions to a specific weight/set/rep.
    /// </summary>
    public class FitnessProgramActionsWsr : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the fitness program action id.
        /// </summary>
        public int FitnessProgramActionId { get; set; }

        /// <summary>
        /// Gets or sets the weight set rep id.
        /// </summary>
        public int WeightSetRepId { get; set; }
    }
}
