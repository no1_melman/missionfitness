﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The fitness program actions.
    /// Joins fitness action to the program
    /// </summary>
    public class FitnessProgramActions : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the fitness program id.
        /// </summary>
        public int FitnessProgramId { get; set; }

        /// <summary>
        /// Gets or sets the fitness action id.
        /// </summary>
        public int FitnessActionId { get; set; }
    }
}
