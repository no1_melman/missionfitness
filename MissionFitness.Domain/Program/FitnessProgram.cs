﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The fitness program.
    /// A program contains actions with sets, reps and weight
    /// </summary>
    public class FitnessProgram : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }
    }
}
