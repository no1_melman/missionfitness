﻿(function() {
    'use strict';

    angular.module('home.module')
        .controller('home.controllers.index', index);

    function index() {
        var vm = this;

        vm.title = "Welcome to Mission Fitness";

        vm.login = function() { console.log("hey"); };
    }
})();