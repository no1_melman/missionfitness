﻿namespace MissionFitness.Gui.Services.Identity.Contracts
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity;

    using MissionFitness.Domain.Identity;

    /// <summary>
    /// The RoleManagerFactory interface.
    /// </summary>
    public interface IRoleManagerFactory
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="RoleManager"/>.
        /// </returns>
        RoleManager<Role, int> Create();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="RoleManager"/>.
        /// </returns>
        RoleManager<Role, int> Create(DbContext context);
    }
}
