﻿namespace MissionFitness.Gui.App_Start.Bundles.BundlePartials
{
    using MissionFitness.Common.Bundling;
    using MissionFitness.Gui.App_Start.Bundles.Formatters;

    /// <summary>
    /// The home bundles.
    /// </summary>
    public static class InterceptionBundles
    {
        /// <summary>
        /// The scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial Scripts()
        {
            return new BundlePartial("Interception")
                .UseFormatter(new ApplicationBlockPathFormatter())
                .AsModule()
                .IncludePath("http");
        }
    }
}