﻿(function() {
    'use strict';

    angular.module('missionfitness', [
        // external modules
        'ui.bootstrap',

        // application blocks
        'blocks.routing',
        'blocks.interception',
        'blocks.logger',
        'blocks.exception',

        // application modules
        'home.module',
        'admin.module',
        'member.module',
        'menu.module',
        'security.module'
    ])
        .config(['$httpProvider', authConfigure])
        .run(['$rootScope', run])
        .constant('toastr', toastr)
        .constant('$', $);

    function authConfigure($httpProvider) {
        $httpProvider.interceptors.push('http.authorisation.interceptor');
    }

    function run($rootScope) {
        $rootScope.$on('$stateChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                
                // transitionTo() promise will be rejected with
                // a 'transition prevented' error
            });
    }
})();