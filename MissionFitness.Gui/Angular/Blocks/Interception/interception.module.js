﻿(function() {
    'use strict';

    angular.module('blocks.interception', ['LocalStorageModule']);
})();