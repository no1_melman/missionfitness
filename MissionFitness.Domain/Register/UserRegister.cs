﻿namespace MissionFitness.Domain.Register
{
    using System;

    /// <summary>
    /// The user register.
    /// </summary>
    public class UserRegister : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether active.
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets the in.
        /// </summary>
        public DateTime In { get; set; }

        /// <summary>
        /// Gets or sets the out.
        /// </summary>
        public DateTime Out { get; set; }
    }
}
