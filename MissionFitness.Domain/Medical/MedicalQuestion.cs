﻿namespace MissionFitness.Domain.Medical
{
    /// <summary>
    /// The medical question.
    /// </summary>
    public class MedicalQuestion : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }
    }
}
