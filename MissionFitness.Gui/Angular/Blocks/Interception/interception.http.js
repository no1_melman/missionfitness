﻿(function() {
    'use strict';

    angular.module('blocks.interception')
        .factory('http.authorisation.interceptor', ['$q', 'localStorageService', '$location', authorisationInterceptor]);

    function authorisationInterceptor($q, localStorageService) {
        var service = {
            request: request,
            responseError: responseError
        };

        return service;

        function request(config) {
            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        }

        function responseError(rejection) {
            if (rejection.status === 401) {
                $location.path('/');
            }
            return $q.reject(rejection);
        }

        function isInRole(rolesNeeded, rolesUserHaves) {
            var success = false;

            success = rolesUserHaves === undefined && rolesNeeded === undefined;

            angular.forEach(rolesUserHaves, function (role) {
                // if there are no roles then we have permission
                if (rolesNeeded === undefined || rolesNeeded.length === 0) {
                    success = true;
                } else {
                    angular.forEach(rolesNeeded, function (neededRole) {
                        if (role === neededRole) {
                            success = true;
                        }
                    });
                }
            });

            return success;
        }
    }

})();