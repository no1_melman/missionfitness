﻿namespace MissionFitness.Gui.App_Start.Registrations
{
    using System.Data.Entity.Infrastructure;

    using AutoMapper;

    using Microsoft.Owin.Security.OAuth;
    using Microsoft.Practices.Unity;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Domain;
    using MissionFitness.Gui.Models.Admin;
    using MissionFitness.Gui.Services.Admin;
    using MissionFitness.Gui.Services.Identity;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The services config.
    /// </summary>
    public class ServiceRegistration
    {
        /// <summary>
        /// The configure.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        public static void Configure(IUnityContainer container)
        {
            ConfigureIdentity(container);

            ConfigureMedicalServices(container);

            // AutoMapper
            container.RegisterType<IMappingEngine>(new InjectionFactory(_ => Mapper.Engine));
        }

        /// <summary>
        /// The configure identity.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        private static void ConfigureIdentity(IUnityContainer container)
        {
            container.RegisterType<IMissionContextFactory, DbContextFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IUserManagerFactory, UserManagerFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IRoleManagerFactory, RoleManagerFactory>(new PerResolveLifetimeManager());
            container.RegisterType<ISignInManagerFactory, SignInManagerFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IAuthenticationManagerFactory, AuthenticationManagerFactory>(new PerResolveLifetimeManager());
            container.RegisterType<IOAuthAuthorizationServerProvider, SimpleAuthorisationProvider>(new PerResolveLifetimeManager());
        }

        /// <summary>
        /// The configure medical services.
        /// </summary>
        /// <param name="container">
        /// The container.
        /// </param>
        private static void ConfigureMedicalServices(IUnityContainer container)
        {
            container.RegisterType<IMedicalProfileService<MedicalQuestionDto, MedicalAnswerDto>, MedicalProfileService>(new PerResolveLifetimeManager());
            container.RegisterType<IMedicalQuestionService<MedicalQuestionDto>, MedicalQuestionService>(new PerResolveLifetimeManager());
        }
    }
}