﻿namespace MissionFitness.Gui.App_Start.Bundles.BundlePartials
{
    using MissionFitness.Common.Bundling;
    using MissionFitness.Gui.App_Start.Bundles.Formatters;

    /// <summary>
    /// The menu bundles.
    /// </summary>
    public static class MenuBundles
    {
        /// <summary>
        /// The scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial Scripts()
        {
            return new BundlePartial("Menu")
                .UseFormatter(new StandardPathFormatter())
                .AsModule()
                .IncludeControllers();
        }
    }
}