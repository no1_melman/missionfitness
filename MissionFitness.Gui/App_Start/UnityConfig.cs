namespace MissionFitness.Gui.App_Start
{
    using System;

    using Microsoft.Practices.Unity;

    using MissionFitness.Gui.App_Start.Registrations;

    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        /// <summary>
        /// The container.
        /// </summary>
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            ServiceRegistration.Configure(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        /// <returns>
        /// The <see cref="IUnityContainer"/>.
        /// </returns>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
    }
}
