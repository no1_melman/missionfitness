﻿namespace MissionFitness.Common.Extensions
{
    /// <summary>
    /// The exception formatting extensions.
    /// </summary>
    public static class ExceptionFormattingExtensions
    {
        /// <summary>
        /// The strip first segment.
        /// </summary>
        /// <param name="value">
        /// The value.
        /// </param>
        /// <param name="breakChar">
        /// The break char.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string StripFirstSegement(this string value, char breakChar)
        {
            var valuesSplit = value.Split(breakChar);

            return valuesSplit.Length == 1 
                ? value // if it hasn't been split then return the value
                : valuesSplit[1]; // if it has been split then return the right side
        }
    }
}
