﻿using Microsoft.Owin;

[assembly: OwinStartupAttribute(typeof(MissionFitness.Gui.Startup))]

namespace MissionFitness.Gui
{
    using System;
    using System.Web.Http;

    using Microsoft.Owin;
    using Microsoft.Owin.Security.OAuth;

    using MissionFitness.Gui.Services.Identity;

    using Owin;

    /// <summary>
    /// The Application Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Gets the simple authorisation provider.
        /// </summary>
        private SimpleAuthorisationProvider SimpleAuthorisationProvider
        {
            get
            {
                return (SimpleAuthorisationProvider)GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(IOAuthAuthorizationServerProvider));
            }
        }

        /// <summary>
        /// The configuration.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public void Configuration(IAppBuilder app)
        {
            this.ConfigureOAuth(app);
        }

        /// <summary>
        /// Configures OAuth Bearer Authentication.
        /// </summary>
        /// <param name="app">
        /// The app.
        /// </param>
        public void ConfigureOAuth(IAppBuilder app)
        {
            var oauthServerOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(1),
                Provider = this.SimpleAuthorisationProvider
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(oauthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }
    }
}