﻿namespace MissionFitness.Common.Exception
{
    using System.Collections.Generic;

    /// <summary>
    /// This is the service exception, it should be thrown if you want to provide more information to the user on what has been thrown.
    /// Generally it should contain a list of errors as strings, to do this create a derived class which converts what ever type you are using
    /// to a list of string <see cref="GuidServiceException"/>. 
    /// </summary>
    public class ServiceException : System.Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        /// <param name="exceptionData">
        /// The exception data.
        /// </param>
        public ServiceException(string message, IEnumerable<string> exceptionData)
            : base(message)
        {
            this.ExceptionData = exceptionData;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ServiceException"/> class.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public ServiceException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Gets or sets the exception data.
        /// </summary>
        public IEnumerable<string> ExceptionData { get; protected set; }
    }
}
