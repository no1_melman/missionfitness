﻿namespace MissionFitness.Gui.Services.Identity
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using MissionFitness.Domain;
    using MissionFitness.Domain.Identity;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The user manager factory.
    /// </summary>
    public class UserManagerFactory : IUserManagerFactory
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="UserManager"/>.
        /// </returns>
        public UserManager<User, int> Create()
        {
            return new UserManager<User, int>(new UserStore<User, Role, int, UserLogin, UserRole, UserClaim>(new MissionContext()));
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="UserManager"/>.
        /// </returns>
        public UserManager<User, int> Create(DbContext context)
        {
            return new UserManager<User, int>(new UserStore<User, Role, int, UserLogin, UserRole, UserClaim>(context));
        }
    }
}