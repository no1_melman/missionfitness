﻿namespace MissionFitness.Common.Bundling
{
    using System.Collections.Generic;

    /// <summary>
    /// The bundle partial extensions.
    /// </summary>
    public static class BundlePartialExtensions
    {
        /// <summary>
        /// The include path.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial IncludePath(this BundlePartial partial, string path)
        {
            if (partial.Paths == null)
            {
                partial.Paths = new List<string>().ToArray();
            }

            var pathsList = new List<string>(partial.Paths) { path };
            partial.Paths = pathsList.ToArray();

            return partial;
        }

        /// <summary>
        /// If the bundle uses the module structure.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial AsModule(this BundlePartial partial)
        {
            if (partial.Paths == null)
            {
                partial.Paths = new List<string>().ToArray();
            }

            var pathsList = new List<string>(partial.Paths) { "module" };
            partial.Paths = pathsList.ToArray();

            return partial;
        }

        /// <summary>
        /// The include services.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial IncludeServices(this BundlePartial partial)
        {
            if (partial.Paths == null)
            {
                partial.Paths = new List<string>().ToArray();
            }

            var pathsList = new List<string>(partial.Paths) { "services" };
            partial.Paths = pathsList.ToArray();

            return partial;
        }

        /// <summary>
        /// The include directives.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial IncludeDirectives(this BundlePartial partial)
        {
            if (partial.Paths == null)
            {
                partial.Paths = new List<string>().ToArray();
            }

            var pathsList = new List<string>(partial.Paths) { "directives" };
            partial.Paths = pathsList.ToArray();

            return partial;
        }

        /// <summary>
        /// The include controllers.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial IncludeControllers(this BundlePartial partial)
        {
            if (partial.Paths == null)
            {
                partial.Paths = new List<string>().ToArray();
            }

            var pathsList = new List<string>(partial.Paths) { "controllers" };
            partial.Paths = pathsList.ToArray();

            return partial;
        }

        /// <summary>
        /// The use formatter.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <param name="formatter">
        /// The formatter.
        /// </param>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial UseFormatter(this BundlePartial partial, IPathFormatter formatter)
        {
            partial.Formatter = formatter;

            return partial;
        }
    }
}
