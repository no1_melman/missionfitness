﻿namespace MissionFitness.Gui.Models
{
    /// <summary>
    /// The primary key.
    /// </summary>
    public class PrimaryKey
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }
    }
}