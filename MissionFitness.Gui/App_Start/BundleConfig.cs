﻿namespace MissionFitness.Gui
{
    using System.Web.Optimization;

    using MissionFitness.Gui.App_Start.Bundles;
    using MissionFitness.Gui.App_Start.Bundles.BundlePartials;

    /// <summary>
    /// The bundle config.
    /// </summary>
    public class BundleConfig
    {
        /// <summary>
        /// The register bundles.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(Bootstrap.Styles());

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css",
                      "~/Content/toastr.css"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/others").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/toastr.js",
                        "~/Scripts/respond.js",
                        "~/Scripts/modernizr-*"));

            bundles.Add(Angular.Scripts());
            bundles.Add(Bootstrap.Scripts());

            CreateAngularAppBundle(bundles);
        }

        /// <summary>
        /// The create angular app bundle.
        /// </summary>
        /// <param name="bundles">
        /// The bundles.
        /// </param>
        private static void CreateAngularAppBundle(BundleCollection bundles)
        {
            var bundlePartials = new[]
                                     {
                                         InterceptionBundles.Scripts(),
                                         RoutingBundles.Scripts(),
                                         LoggingBundle.Scripts(),
                                         ExceptionBundle.Scripts(),
                                         MenuBundles.Scripts(),
                                         HomeBundles.Scripts(), 
                                         AdminBundles.Scripts(),
                                         SecurityBundles.Scripts(),
                                         MemberBundles.Scripts()
                                     };

            bundles.Add(
                BundleBuilder.Build(
                    bundleName: "angularApp",
                    angularAppPath: "app",
                    bundlePartials: bundlePartials));
        }
    }
}
