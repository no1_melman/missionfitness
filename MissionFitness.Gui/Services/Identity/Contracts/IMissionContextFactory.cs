﻿namespace MissionFitness.Gui.Services.Identity.Contracts
{
    using System.Data.Entity.Infrastructure;

    using MissionFitness.Domain;

    /// <summary>
    /// The MissionContextFactory interface.
    /// </summary>
    public interface IMissionContextFactory : IDbContextFactory<MissionContext>
    {
    }
}
