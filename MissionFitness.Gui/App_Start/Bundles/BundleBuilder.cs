﻿namespace MissionFitness.Gui.App_Start.Bundles
{
    using System.Collections.Generic;
    using System.Text;
    using System.Web.Optimization;

    using MissionFitness.Common.Bundling;

    /// <summary>
    /// The bundle builder.
    /// </summary>
    public static class BundleBuilder
    {
        /// <summary>
        /// The build.
        /// </summary>
        /// <param name="bundleName">
        /// The bundle name.
        /// </param>
        /// <param name="bundlePartials">
        /// The bundle partials.
        /// </param>
        /// <param name="angularAppPath">
        /// The Angular App Path.
        /// </param>
        /// <returns>
        /// The <see cref="Bundle"/>.
        /// </returns>
        public static Bundle Build(string bundleName, BundlePartial[] bundlePartials, string angularAppPath)
        {
            var virtualPaths = new List<string>();

            foreach (var bundlePartial in bundlePartials)
            {
                virtualPaths.AddRange(bundlePartial.Build());
            }

            virtualPaths.Add(string.Format("~/Angular/{0}.js", angularAppPath));

            var bundlePath = new StringBuilder();
            bundlePath.Append(@"~/bundles/");
            bundlePath.Append(bundleName.ToLower());

            return new ScriptBundle(bundlePath.ToString()).Include(virtualPaths.ToArray());
        }
    }
}