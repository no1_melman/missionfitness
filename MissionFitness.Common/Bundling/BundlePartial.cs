﻿namespace MissionFitness.Common.Bundling
{
    using System.Collections.Generic;

    /// <summary>
    /// The bundle partial.
    /// </summary>
    public class BundlePartial
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BundlePartial"/> class.
        /// </summary>
        /// <param name="area">
        /// The area.
        /// </param>
        /// <param name="paths">
        /// The paths.
        /// </param>
        public BundlePartial(string area, string[] paths)
        {
            this.Area = area;
            this.Paths = paths;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BundlePartial"/> class.
        /// </summary>
        /// <param name="area">
        /// The area.
        /// </param>
        public BundlePartial(string area)
        {
            this.Area = area;
        }

        /// <summary>
        /// Gets or sets the formatter.
        /// </summary>
        public IPathFormatter Formatter { get; set; }

        /// <summary>
        /// Gets or sets the area.
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Gets or sets the paths.
        /// </summary>
        public string[] Paths { get; set; }

        /// <summary>
        /// The build.
        /// </summary>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<string> Build()
        {
            return this.Formatter.Format(this);
        }

    }
}
