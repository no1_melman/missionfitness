﻿namespace MissionFitness.Gui.Services.Identity
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;

    using MissionFitness.Domain;
    using MissionFitness.Domain.Identity;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The role manager factory.
    /// </summary>
    public class RoleManagerFactory : IRoleManagerFactory
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="RoleManager"/>.
        /// </returns>
        public RoleManager<Role, int> Create()
        {
            return new RoleManager<Role, int>(new RoleStore<Role, int, UserRole>(new MissionContext()));
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="RoleManager"/>.
        /// </returns>
        public RoleManager<Role, int> Create(DbContext context)
        {
            return new RoleManager<Role, int>(new RoleStore<Role, int, UserRole>(context));
        }
    }
}