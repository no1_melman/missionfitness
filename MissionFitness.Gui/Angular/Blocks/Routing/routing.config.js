﻿(function() {
    'use strict';

    angular.module('blocks.routing')
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', routingConfig]);

    function routingConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider.state('main', {
            url: '/',
            views: {
                "shell": { templateUrl: "/Partials/Shell.html" },
                "mainContent@main": { templateUrl: "/Partials/Home/Index.html", controller: "home.controllers.index as vm" },
                "menu@": { templateUrl: "/Partials/Menu/Default.html", controller: "menu.controllers.default as vm" }
            }
        });

        $stateProvider.state('home', {
            url: '',
            abstract: true,
            views: {
                "shell": { templateUrl: "/Partials/Shell.html" },
                "menu@": { templateUrl: "/Partials/Menu/Default.html", controller: "menu.controllers.default as vm" }
            }
        });

        $stateProvider.state('admin', {
            url: '/Admin',
            abstract: true,
            views: {
                "shell": { templateUrl: "/Partials/Shell.html" },
                "menu@": { templateUrl: "/Partials/Menu/Default.html", controller: "menu.controllers.default as vm" }
            }
        });


        $stateProvider.state('home.index', {
            url: 'Index',
            views: {
                "mainContent@main": { templateUrl: "/Partials/Home/Index.html", controller: "home.controllers.index as vm" }
            }
        });

        $stateProvider.state('admin.memberRegistration', {
            url: '/MemberRegistration',
            views: {
                "mainContent@admin": { templateUrl: '/Partials/Admin/RegisterMember.html', controller: 'admin.controllers.memberRegistration as vm' }
            }
        });

        $stateProvider.state('admin.medicalProfile', {
            url: '/MedicalProfile',
            views: {
                "mainContent@admin": { templateUrl: '/Partials/Admin/MedicalProfile.html', controller: 'admin.controllers.medicalProfile as vm' }
            }
        });

        $stateProvider.state('default', {
            url: '*all',
            views: {
                "shell": { templateUrl: "/Partials/Shell.html" },
                "menu@": { templateUrl: "/Partials/Menu/Default.html", controller: "menu.controllers.default as vm" },
                "mainContent@default": { templateUrl: "/Partials/Default/Default.html" }
            }
        });

        // Miscellaneous
        $urlRouterProvider.otherwise('/');

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
    };
})()