﻿namespace MissionFitness.Gui.Services.Identity
{
    using System.Threading.Tasks;

    using Microsoft.AspNet.Identity.Owin;
    using Microsoft.Owin.Security.OAuth;

    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The simple authorisation provider.
    /// </summary>
    public class SimpleAuthorisationProvider : OAuthAuthorizationServerProvider
    {
        /// <summary>
        /// The sign in manager factory.
        /// </summary>
        private readonly ISignInManagerFactory signInManagerFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SimpleAuthorisationProvider"/> class.
        /// </summary>
        /// <param name="signInManagerFactory">
        /// The sign in manager factory.
        /// </param>
        public SimpleAuthorisationProvider(
            ISignInManagerFactory signInManagerFactory)
        {
            this.signInManagerFactory = signInManagerFactory;
        }

        /// <summary>
        /// The validate client authentication.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        /// <summary>
        /// The grant resource owner credentials.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            using (var signInManager = this.signInManagerFactory.Create())
            {
                var signInStatus = await signInManager.PasswordSignInAsync(context.UserName, context.Password, true, false);

                if (signInStatus != SignInStatus.Success)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                var user = await signInManager.UserManager.FindByNameAsync(context.UserName);
                var identity = await signInManager.CreateUserIdentityAsync(user);

                context.Validated(identity);
            }
        }
    }
}