﻿namespace MissionFitness.Gui.Services.Identity
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity.Owin;

    using MissionFitness.Domain.Identity;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The sign in manager factory.
    /// </summary>
    public class SignInManagerFactory : ISignInManagerFactory
    {
        /// <summary>
        /// The user manager factory.
        /// </summary>
        private readonly IUserManagerFactory userManagerFactory;

        /// <summary>
        /// The authentication manager factory.
        /// </summary>
        private readonly IAuthenticationManagerFactory authenticationManagerFactory;

        /// <summary>
        /// Initializes a new instance of the <see cref="SignInManagerFactory"/> class.
        /// </summary>
        /// <param name="userManagerFactory">
        /// The user manager factory.
        /// </param>
        /// <param name="authenticationManagerFactory">
        /// The authentication manager factory.
        /// </param>
        public SignInManagerFactory(
            IUserManagerFactory userManagerFactory,
            IAuthenticationManagerFactory authenticationManagerFactory)
        {
            this.userManagerFactory = userManagerFactory;
            this.authenticationManagerFactory = authenticationManagerFactory;
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="SignInManager"/>.
        /// </returns>
        public SignInManager<User, int> Create()
        {
            return new SignInManager<User, int>(this.userManagerFactory.Create(), this.authenticationManagerFactory.Create());
        }

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="SignInManager"/>.
        /// </returns>
        public SignInManager<User, int> Create(DbContext context)
        {
            return new SignInManager<User, int>(this.userManagerFactory.Create(context), this.authenticationManagerFactory.Create());
        }
    }
}