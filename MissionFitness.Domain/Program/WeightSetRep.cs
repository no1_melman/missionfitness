﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The weight set rep.
    /// </summary>
    public class WeightSetRep : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the weight.
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Gets or sets the sets.
        /// </summary>
        public int Sets { get; set; }

        /// <summary>
        /// Gets or sets the reps.
        /// </summary>
        public int Reps { get; set; }
    }
}
