﻿namespace MissionFitness.Gui.App_Start.Bundles.BundlePartials
{
    using MissionFitness.Common.Bundling;
    using MissionFitness.Gui.App_Start.Bundles.Formatters;

    /// <summary>
    /// The home bundles.
    /// </summary>
    public static class AdminBundles
    {
        /// <summary>
        /// The scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial Scripts()
        {
            return new BundlePartial("Admin")
                .UseFormatter(new StandardPathFormatter())
                .AsModule()
                .IncludeServices()
                .IncludeControllers();
        }
    }
}