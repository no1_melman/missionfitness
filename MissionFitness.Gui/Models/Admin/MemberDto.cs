﻿namespace MissionFitness.Gui.Models.Admin
{
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// The member DTO.
    /// </summary>
    public class MemberDto
    {
        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        [Required, MinLength(2)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the last name.
        /// </summary>
        [Required, MinLength(2)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        [Required, MinLength(4), EmailAddress]
        public string Email { get; set; }
    }
}