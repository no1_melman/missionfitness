﻿namespace MissionFitness.Core.Services.Admin
{
    using System.Threading.Tasks;

    /// <summary>
    /// The MedicalQuestionService interface.
    /// </summary>
    /// <typeparam name="TMedicalQuestionDto">
    /// The medical question DTO
    /// </typeparam>
    public interface IMedicalQuestionService<TMedicalQuestionDto>
        where TMedicalQuestionDto : class
    {
        /// <summary>
        /// The create medical question.
        /// </summary>
        /// <param name="medicalQuestionDto">
        /// The medical question DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task CreateMedicalQuestion(TMedicalQuestionDto medicalQuestionDto);

        /// <summary>
        /// The delete medical question.
        /// </summary>
        /// <param name="medicalQuestionId">
        /// The medical question id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task DeleteMedicalQuestion(int medicalQuestionId);
    }
}
