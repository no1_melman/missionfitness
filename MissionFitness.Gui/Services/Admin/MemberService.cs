﻿namespace MissionFitness.Gui.Services.Admin
{
    using System.Threading.Tasks;

    using AutoMapper;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Domain.Identity;
    using MissionFitness.Gui.Models.Admin;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The member service.
    /// </summary>
    public class MemberService : IMemberService<MemberDto>
    {
        /// <summary>
        /// The context factory.
        /// </summary>
        private readonly IMissionContextFactory contextFactory;

        /// <summary>
        /// The user manager factory.
        /// </summary>
        private readonly IUserManagerFactory userManagerFactory;

        /// <summary>
        /// The mapping engine.
        /// </summary>
        private readonly IMappingEngine mappingEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberService"/> class.
        /// </summary>
        /// <param name="contextFactory">
        /// The context factory.
        /// </param>
        /// <param name="userManagerFactory">
        /// The user manager factory.
        /// </param>
        /// <param name="mappingEngine">
        /// The mapping engine.
        /// </param>
        public MemberService(
            IMissionContextFactory contextFactory,
            IUserManagerFactory userManagerFactory,
            IMappingEngine mappingEngine)
        {
            this.contextFactory = contextFactory;
            this.userManagerFactory = userManagerFactory;
            this.mappingEngine = mappingEngine;
        }

        /// <summary>
        /// The register member async.
        /// </summary>
        /// <param name="memberDto">
        /// The member DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task RegisterMemberAsync(MemberDto memberDto)
        {
            using (var userManager = this.userManagerFactory.Create())
            {
                var result = await userManager.CreateAsync(this.mappingEngine.Map<User>(memberDto));

                if (!result.Succeeded)
                {
                    
                }
            }
        }
    }
}