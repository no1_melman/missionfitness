﻿namespace MissionFitness.Gui.Services.Identity.Contracts
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity.Owin;

    using MissionFitness.Domain.Identity;

    /// <summary>
    /// The sign in manager factory interface.
    /// </summary>
    public interface ISignInManagerFactory
    {
        /// <summary>
        /// Constructs a Sign in Manager.
        /// </summary>
        /// <returns>
        /// The <see cref="SignInManager"/>.
        /// </returns>
        SignInManager<User, int> Create();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="SignInManager"/>.
        /// </returns>
        SignInManager<User, int> Create(DbContext context);
    }
}