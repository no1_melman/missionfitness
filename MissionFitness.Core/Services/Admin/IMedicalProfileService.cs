﻿namespace MissionFitness.Core.Services.Admin
{
    using System.Collections.Generic;
    using System.Security.Principal;
    using System.Threading.Tasks;

    /// <summary>
    /// The MedicalProfileService interface.
    /// </summary>
    /// <typeparam name="TMedicalQuestionDto">
    /// The medical question DTO
    /// </typeparam>
    /// <typeparam name="TMedicalAnswerDto">
    /// The medical answer DTO
    /// </typeparam>
    public interface IMedicalProfileService<TMedicalQuestionDto, TMedicalAnswerDto>
        where TMedicalQuestionDto : class
        where TMedicalAnswerDto : class
    {
        /// <summary>
        /// The get questions async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TMedicalQuestionDto>> GetQuestionsAsync();

        /// <summary>
        /// The get answers async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task<List<TMedicalAnswerDto>> GetAnswersAsync();

        /// <summary>
        /// The save answers async.
        /// </summary>
        /// <param name="medicalProfileId">
        /// The medical profile id.
        /// </param>
        /// <param name="medicalAnswerDtos">
        /// The medical Answer DTOs.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task SaveAnswersAsync(int medicalProfileId, List<TMedicalAnswerDto> medicalAnswerDtos);

        /// <summary>
        /// Gets or sets the user.
        /// </summary>
        IPrincipal User { get; set; }
    }
}
