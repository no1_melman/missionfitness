﻿(function() {
    'use strict';

    angular.module('security.module')
        .factory('security.services.authorisation', ['$http', '$q', 'localStorageService', authorisationService]);

    function authorisationService($http, $q, localStorageService) {
        var authentication = {
            isAuth: false,
            userName: "",
            roles: []
        };

        var service = {
            login: login,
            logOut: logOut,
            getRoles: getRoles,
            isInRole: isInRole,
            fillAuthData: fillAuthData,
            authentication: authentication
        };

        return service;

        function login(loginData) {
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();

            $http.post('/Token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
                var token = response.access_token;

                localStorageService.set('authorizationData', { token: token, userName: loginData.userName});

                authentication.isAuth = true;
                authentication.userName = loginData.userName;

                deferred.resolve(true);
            }).error(function (err) {
                logOut();
                deferred.reject(err);
            });

            return deferred.promise;
        }

        function logOut() {
            localStorageService.remove('authorizationData');

            authentication.isAuth = false;
            authentication.userName = "";
        }

        function fillAuthData() {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                authentication.isAuth = true;
                authentication.userName = authData.userName;
            }
        }

        function getRoles() {
            var authData = localStorageService.get('authorizationData');
            if (authData) {
                return authData.roles;
            }
        }

        function isInRole(rolesNeeded, rolesUserHaves) {
            var success = false;

            success = rolesUserHaves === undefined && rolesNeeded === undefined;

            angular.forEach(rolesUserHaves, function (role) {
                // if there are no roles then we have permission
                if (rolesNeeded === undefined || rolesNeeded.length === 0) {
                    success = true;
                } else {
                    angular.forEach(rolesNeeded, function (neededRole) {
                        if (role === neededRole) {
                            success = true;
                        }
                    });
                }
            });

            return success;
        }
    }
})();