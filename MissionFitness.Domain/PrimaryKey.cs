﻿namespace MissionFitness.Domain
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    /// <summary>
    /// The primary key.
    /// </summary>
    public abstract class PrimaryKey
    {
        /// <summary>
        /// Gets or sets the key.
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public int Id { get; set; }
    }
}
