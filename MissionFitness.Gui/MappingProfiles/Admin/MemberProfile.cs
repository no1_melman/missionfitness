﻿namespace MissionFitness.Gui.MappingProfiles.Admin
{
    using AutoMapper;

    using MissionFitness.Domain.Identity;
    using MissionFitness.Gui.Models.Admin;

    /// <summary>
    /// The member profile.
    /// </summary>
    public class MemberProfile : Profile
    {
        /// <summary>
        /// Gets the profile name.
        /// </summary>
        public override string ProfileName
        {
            get
            {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<MemberDto, User>()
                .ForMember(u => u.UserName, c => c.MapFrom(m => m.Email));
        }
    }
}