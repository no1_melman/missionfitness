﻿namespace MissionFitness.Common.Bundling
{
    using System.Collections.Generic;

    /// <summary>
    /// The PathFormatter interface.
    /// </summary>
    public interface IPathFormatter
    {
        /// <summary>
        /// The format.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        List<string> Format(BundlePartial partial);
    }
}
