﻿namespace MissionFitness.Gui.Models.Admin
{
    using System.Collections.Generic;

    /// <summary>
    /// The medical answer dto.
    /// </summary>
    public class MedicalAnswerDto : PrimaryKey
    {
        /// <summary>
        /// Gets or sets a value indicating whether answer.
        /// </summary>
        public bool Answer { get; set; }

        /// <summary>
        /// Gets or sets the medical question id.
        /// </summary>
        public int MedicalQuestionId { get; set; }

        /// <summary>
        /// Gets or sets the medical profile id.
        /// </summary>
        public int MedicalProfileId { get; set; }

        /// <summary>
        /// Gets or sets the medical question.
        /// </summary>
        public MedicalQuestionDto MedicalQuestion { get; set; } 
    }
}