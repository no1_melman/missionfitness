﻿namespace MissionFitness.Gui.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;

    using MissionFitness.Common.Exception;
    using MissionFitness.Common.Extensions;
    using MissionFitness.Gui.Models.Response;

    /// <summary>
    /// The service API controller.
    /// </summary>
    public class ServiceApiController : ApiController
    {
        /// <summary>
        /// The execute query.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <typeparam name="T">
        /// The returned result
        /// </typeparam>
        /// <returns>
        /// The <see cref="JsonResponse{T}"/>.
        /// </returns>
        protected async Task<JsonResponse<T>> ExecuteQuery<T>(Task<List<T>> service)
            where T : class
        {
            var success = false;
            var message = string.Empty;
            var data = new List<T>();
            var errors = new List<string>();
            JsonResponse<T> response;

            if (this.ModelState.IsValid)
            {
                List<string> errorList = null;
                try
                {
                    data = await service;

                    success = true;
                }
                catch (ServiceException serviceException)
                {
                    errorList = serviceException.ExceptionData.ToList();
                    message = serviceException.Message;
                }
                catch (Exception exception)
                {
                    message = exception.Message;
                }

                response = errorList == null ? new JsonResponse<T>(message, success, dataList: data) : new JsonResponse<T>(message, success, errors: errorList);
            }
            else
            {
                message = "Validation Errors";
                errors.AddRange(this.ModelState.SelectMany(x => x.Value.Errors.Select(y => string.Format("{0} : {1}", x.Key.StripFirstSegement('.'), y.ErrorMessage))));
                response = new JsonResponse<T>(message, success, errors: errors);
            }

            return response;
        }

        /// <summary>
        /// The execute query.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <typeparam name="T">
        /// The returned result
        /// </typeparam>
        /// <returns>
        /// The <see cref="JsonResponse{T}"/>.
        /// </returns>
        protected async Task<JsonResponse<T>> ExecuteQuery<T>(Task<T> service)
            where T : class
        {
            var success = false;
            var message = string.Empty;
            var data = Activator.CreateInstance<T>();
            var errors = new List<string>();
            JsonResponse<T> response;

            if (this.ModelState.IsValid)
            {
                List<string> errorList = null;
                try
                {
                    data = await service;

                    success = true;
                }
                catch (ServiceException serviceException)
                {
                    errorList = serviceException.ExceptionData.ToList();
                    message = serviceException.Message;
                }
                catch (Exception exception)
                {
                    message = exception.Message;
                }

                response = errorList == null ? new JsonResponse<T>(message, success, data: data) : new JsonResponse<T>(message, success, errors: errorList);
            }
            else
            {
                message = "Validation Errors";
                errors.AddRange(this.ModelState.SelectMany(x => x.Value.Errors.Select(y => string.Format("{0} : {1}", x.Key.StripFirstSegement('.'), y.ErrorMessage))));
                response = new JsonResponse<T>(message, success, errors: errors);
            }

            return response;
        }

        /// <summary>
        /// The execute service.
        /// </summary>
        /// <param name="service">
        /// The service.
        /// </param>
        /// <returns>
        /// The <see cref="JsonResponse{T}"/>.
        /// </returns>
        protected async Task<JsonResponse<object>> ExecuteService(Task service)
        {
            var success = false;
            var message = string.Empty;
            var errors = new List<string>();
            JsonResponse<object> response;

            if (this.ModelState.IsValid)
            {
                var errorList = new List<string>();
                try
                {
                    await service;

                    success = true;
                }
                catch (ServiceException serviceException)
                {
                    errorList = serviceException.ExceptionData.ToList();
                    message = serviceException.Message;
                }
                catch (Exception exception)
                {
                    message = exception.Message;
                }

                response = new JsonResponse<object>(message, success, errors: errorList);
            }
            else
            {
                message = "Validation Errors";
                errors.AddRange(this.ModelState.SelectMany(x => x.Value.Errors.Select(y => string.Format("{0} : {1}", x.Key.StripFirstSegement('.'), y.ErrorMessage))));
                response = new JsonResponse<object>(message, success, errors: errors);
            }

            return response;
        }
    }
}
