﻿(function() {
    'use strict';

    angular.module('security.module')
        .controller('security.controllers.login', ['$modalInstance', 'security.services.authorisation', loginController]);

    function loginController($modalInstance, authorisationService) {
        var vm = this;

        // public properties
        vm.loginDetails = {};
        vm.loggedIn = false;

        // public methods
        vm.login = login;
        vm.close = close;
        
        // implementations
        function login() {
            authorisationService.login(vm.loginDetails).then(function () {
                vm.loggedIn = true;

                $modalInstance.close({loggedIn: true, username: vm.loginDetails.userName});
            }).catch(function (error) {
            });
        };

        function close() {
            $modalInstance.dismiss('cancel');
        };
    }
})();