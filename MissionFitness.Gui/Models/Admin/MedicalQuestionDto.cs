﻿namespace MissionFitness.Gui.Models.Admin
{
    /// <summary>
    /// The medical question dto.
    /// </summary>
    public class MedicalQuestionDto : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the question.
        /// </summary>
        public string Question { get; set; }
    }
}