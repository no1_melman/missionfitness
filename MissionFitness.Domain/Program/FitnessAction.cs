﻿namespace MissionFitness.Domain.Program
{
    /// <summary>
    /// The fitness action.
    /// This is just the action that is performed: lateral raisers etc.
    /// </summary>
    public class FitnessAction : PrimaryKey
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }
    }
}
