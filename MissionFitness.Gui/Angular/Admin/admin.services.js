﻿(function() {
    'use strict';

    angular.module('admin.module')
        .factory('admin.services.memberService', ['$http', 'exception', memberService])
        .factory('admin.services.medicalProfile', ['$http', 'exception', medicalProfileService]);

    function memberService($http, exception) {
        var service = {
            registerMember: registerMember
        };

        var serviceBase = '/Api/Member';

        return service;

        function registerMember(member) {
            return $http.post(serviceBase, member)
                .then(function(data) {
                    return data.data;
                })
                .catch(function (message) {
                    exception.catcher('XHR failed for registerMember')(message);
                });
        }
    }

    function medicalProfileService($http, exception) {
        var service = {
            get: get,
            getQuestions: getQuestions,
            save: save
        };

        var serviceBase = '/Api/MedicalProfile';

        return service;

        function get() {
            return $http.get(serviceBase)
		        .then(function (data) {
		            return data.data;
		        })
		        .catch(function (message) {
		            exception.catcher('XHR failed for getMedicalProfile')(message);
		        });
        }

        function getQuestions() {
            return $http.get(serviceBase + '/Questions')
		        .then(function (data) {
		            return data.data;
		        })
		        .catch(function (message) {
		            exception.catcher('XHR failed for getQuestions')(message);
		        });
        }

        function save(medicalAnswers) {
            return $http.post(serviceBase, { medicalAnswersPostDto: medicalAnswers }).
		        then(function (data) {
		            return data.data;
		        })
		        .catch(function (message) {
		            exception.catcher('XHR failed for saveMedicalProfile')(message);
		        });
        }
    }
})();