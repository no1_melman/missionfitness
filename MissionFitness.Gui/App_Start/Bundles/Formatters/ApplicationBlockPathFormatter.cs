﻿namespace MissionFitness.Gui.App_Start.Bundles.Formatters
{
    using System.Collections.Generic;
    using System.Text;

    using MissionFitness.Common.Bundling;

    /// <summary>
    /// The standard path formatter.
    /// </summary>
    public class ApplicationBlockPathFormatter : IPathFormatter
    {
        /// <summary>
        /// The format.
        /// </summary>
        /// <param name="partial">
        /// The partial.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public List<string> Format(BundlePartial partial)
        {
            var virtualPaths = new List<string>();

            foreach (var file in partial.Paths)
            {
                var bundle = new StringBuilder();
                bundle.Append(@"~/Angular/Blocks/");
                bundle.Append(partial.Area);
                bundle.Append(@"/");
                bundle.Append(partial.Area.ToLower());
                bundle.Append(".");
                bundle.Append(file);
                bundle.Append(".js");

                virtualPaths.Add(bundle.ToString());
            }

            return virtualPaths;
        }
    }
}