﻿namespace MissionFitness.Gui.MappingProfiles.Admin
{
    using AutoMapper;

    using MissionFitness.Domain.Medical;
    using MissionFitness.Gui.Models.Admin;

    /// <summary>
    /// The medical profile profile.
    /// </summary>
    public class MedicalProfileProfile : Profile
    {
        /// <summary>
        /// Gets the profile name.
        /// </summary>
        public override string ProfileName
        {
            get
            {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<MedicalQuestion, MedicalQuestionDto>();
            Mapper.CreateMap<MedicalQuestionDto, MedicalQuestion>();

            Mapper.CreateMap<MedicalAnswer, MedicalAnswerDto>()
                .ForMember(m => m.MedicalQuestion, c => c.Ignore());
            Mapper.CreateMap<MedicalAnswerDto, MedicalAnswer>();
        }
    }
}