﻿namespace MissionFitness.Gui.App_Start.Bundles
{
    using System.Web.Optimization;

    /// <summary>
    /// The bootstrap.
    /// </summary>
    public static class Bootstrap
    {
        /// <summary>
        /// The styles.
        /// </summary>
        /// <returns>
        /// The <see cref="Bundle"/>.
        /// </returns>
        public static Bundle Styles()
        {
            return new StyleBundle("~/Content/bootstrap/css")
                .Include("~/Content/bootstrap.css");
        }

        /// <summary>
        /// Bootstrap scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="Bundle"/>.
        /// </returns>
        public static Bundle Scripts()
        {
            return new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/angular-ui/ui-bootstrap-tpls.js");
        }
    }
}