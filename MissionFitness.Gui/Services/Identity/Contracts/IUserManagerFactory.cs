﻿namespace MissionFitness.Gui.Services.Identity.Contracts
{
    using System.Data.Entity;

    using Microsoft.AspNet.Identity;

    using MissionFitness.Domain.Identity;

    /// <summary>
    /// The UserManagerFactory interface.
    /// </summary>
    public interface IUserManagerFactory
    {
        /// <summary>
        /// The create.
        /// </summary>
        /// <returns>
        /// The <see cref="UserManager"/>.
        /// </returns>
        UserManager<User, int> Create();

        /// <summary>
        /// The create.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The <see cref="UserManager"/>.
        /// </returns>
        UserManager<User, int> Create(DbContext context);
    }
}