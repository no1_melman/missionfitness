﻿namespace MissionFitness.Gui.App_Start.Bundles.BundlePartials
{
    using MissionFitness.Common.Bundling;
    using MissionFitness.Gui.App_Start.Bundles.Formatters;

    /// <summary>
    /// The logging bundle.
    /// </summary>
    public static class LoggingBundle
    {
        /// <summary>
        /// The scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="BundlePartial"/>.
        /// </returns>
        public static BundlePartial Scripts()
        {
            return new BundlePartial("Logging")
                .UseFormatter(new ApplicationBlockPathFormatter())
                .AsModule()
                .IncludeServices();
        }
    }
}