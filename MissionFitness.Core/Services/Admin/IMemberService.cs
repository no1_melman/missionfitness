﻿namespace MissionFitness.Core.Services.Admin
{
    using System.Threading.Tasks;

    /// <summary>
    /// The MemberService interface.
    /// </summary>
    /// <typeparam name="TMemberDto">
    /// </typeparam>
    public interface IMemberService<TMemberDto>
        where TMemberDto : class
    {
        /// <summary>
        /// The register member async.
        /// </summary>
        /// <param name="memberDto">
        /// The member DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        Task RegisterMemberAsync(TMemberDto memberDto);
    }
}