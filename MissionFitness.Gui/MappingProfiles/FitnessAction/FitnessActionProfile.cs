﻿namespace MissionFitness.Gui.MappingProfiles.FitnessAction
{
    using AutoMapper;

    using MissionFitness.Domain.Program;
    using MissionFitness.Gui.Models.FitnessAction;

    /// <summary>
    /// The fitness action profile.
    /// </summary>
    public class FitnessActionProfile : Profile
    {
        /// <summary>
        /// Gets the profile name.
        /// </summary>
        public override string ProfileName
        {
            get
            {
                return this.GetType().Name;
            }
        }

        /// <summary>
        /// The configure.
        /// </summary>
        protected override void Configure()
        {
            Mapper.CreateMap<FitnessAction, FitnessActionDto>();
            Mapper.CreateMap<FitnessActionDto, FitnessAction>();
        }
    }
}