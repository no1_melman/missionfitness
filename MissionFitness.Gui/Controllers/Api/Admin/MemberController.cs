﻿namespace MissionFitness.Gui.Controllers.Api.Admin
{
    using System.Threading.Tasks;

    using MissionFitness.Core.Services.Admin;
    using MissionFitness.Gui.Models.Admin;
    using MissionFitness.Gui.Models.Response;

    /// <summary>
    /// The member controller.
    /// </summary>
    public class MemberController : ServiceApiController
    {
        /// <summary>
        /// The member service.
        /// </summary>
        private readonly IMemberService<MemberDto> memberService;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemberController"/> class.
        /// </summary>
        /// <param name="memberService">
        /// The member service.
        /// </param>
        public MemberController(
            IMemberService<MemberDto> memberService)
        {
            this.memberService = memberService;
        }

        /// <summary>
        /// The post async.
        /// </summary>
        /// <param name="memberDto">
        /// The member DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<JsonResponse<object>> PostAsync(MemberDto memberDto)
        {
            return await this.ExecuteService(this.memberService.RegisterMemberAsync(memberDto));
        }
    }
}
