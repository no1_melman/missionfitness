﻿namespace MissionFitness.Gui.Services.Identity.Contracts
{
    using Microsoft.Owin.Security;

    /// <summary>
    /// The Authentication Manager Factory interface.
    /// </summary>
    public interface IAuthenticationManagerFactory
    {
        /// <summary>
        /// Constructs an Authentication Manager.
        /// </summary>
        /// <returns>
        /// The <see cref="IAuthenticationManager"/>.
        /// </returns>
        IAuthenticationManager Create();
    }
}
