﻿(function() {
    'use strict';

    angular.module('menu.module')
        .controller('menu.controllers.default', ['$modal', 'security.services.authorisation', defaultController]);

    function defaultController($modal, authorisationService) {
        var vm = this;

        vm.loggedIn = false;
        vm.username = "";

        authorisationService.fillAuthData();

        if (authorisationService.authentication.isAuth) {
            vm.loggedIn = true;
            vm.username = authorisationService.authentication.userName;
        }

        vm.logout = logout;
        vm.login = login;

        function logout() {
            authorisationService.logOut();

            vm.loggedIn = false;
        }

        function login() {
            var modalInstance = $modal.open({
                templateUrl: '/Partials/Security/Login.html',
                controller: 'security.controllers.login as vm',
            });

            modalInstance.result.then(function (loginDetails) {
                vm.loggedIn = loginDetails.loggedIn;
                vm.username = loginDetails.username;
            });
        }
    }
})();