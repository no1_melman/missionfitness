﻿namespace MissionFitness.Domain.Medical
{
    /// <summary>
    /// The medical answer.
    /// </summary>
    public class MedicalAnswer : PrimaryKey
    {
        /// <summary>
        /// Gets or sets a value indicating whether answer.
        /// </summary>
        public bool Answer { get; set; }

        /// <summary>
        /// Gets or sets the medical question id.
        /// </summary>
        public int MedicalQuestionId { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }
    }
}
