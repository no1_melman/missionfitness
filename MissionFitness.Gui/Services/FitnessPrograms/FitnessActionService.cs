﻿namespace MissionFitness.Gui.Services.FitnessPrograms
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MissionFitness.Core.Services.FitnessPrograms;
    using MissionFitness.Domain.Program;
    using MissionFitness.Gui.Models.FitnessAction;
    using MissionFitness.Gui.Services.Identity.Contracts;

    /// <summary>
    /// The fitness action service.
    /// </summary>
    public class FitnessActionService : IFitnessActionService<FitnessActionDto>
    {
        /// <summary>
        /// The context factory.
        /// </summary>
        private readonly IMissionContextFactory contextFactory;

        /// <summary>
        /// The mapping engine.
        /// </summary>
        private readonly IMappingEngine mappingEngine;

        /// <summary>
        /// Initializes a new instance of the <see cref="FitnessActionService"/> class.
        /// </summary>
        /// <param name="contextFactory">
        /// The context factory.
        /// </param>
        /// <param name="mappingEngine">
        /// The mapping Engine.
        /// </param>
        public FitnessActionService(
            IMissionContextFactory contextFactory,
            IMappingEngine mappingEngine)
        {
            this.contextFactory = contextFactory;
            this.mappingEngine = mappingEngine;
        }

        /// <summary>
        /// The save fitness action async.
        /// </summary>
        /// <param name="fitnessActionDto">
        /// The fitness action DTO.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task SaveFitnessActionAsync(FitnessActionDto fitnessActionDto)
        {
            using (var context = this.contextFactory.Create())
            {
                var action = await context.FitnessActions.FirstOrDefaultAsync(f => f.Id == fitnessActionDto.Id);

                if (action == null)
                {
                    context.FitnessActions.Add(this.mappingEngine.Map<FitnessAction>(fitnessActionDto));
                }
                else
                {
                    this.mappingEngine.Map(fitnessActionDto, action);
                }

                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// The delete fitness action async.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task DeleteFitnessActionAsync(int id)
        {
            using (var context = this.contextFactory.Create())
            {
                var action = await context.FitnessActions.FirstOrDefaultAsync(f => f.Id == id);

                if (action == null)
                {
                    throw new Exception("No action found");
                }

                var fitnessProgramActions = await context.FitnessProgramActionses.Where(f => f.FitnessActionId == id).ToListAsync();
                var fitnessProgramActionWse = await context.FitnessProgramActionsWsrs.Where(f => fitnessProgramActions.Any(p => p.Id == f.FitnessProgramActionId)).ToListAsync();

                context.FitnessActions.Remove(action);

                foreach (var fitnessProgramAction in fitnessProgramActions)
                {
                    context.FitnessProgramActionses.Remove(fitnessProgramAction);
                }

                foreach (var actionsWsr in fitnessProgramActionWse)
                {
                    context.FitnessProgramActionsWsrs.Remove(actionsWsr);
                }
                
                await context.SaveChangesAsync();
            }
        }

        /// <summary>
        /// The get all async.
        /// </summary>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<FitnessActionDto>> GetAllAsync()
        {
            using (var context = this.contextFactory.Create())
            {
                return await context.FitnessActions
                    .Project()
                    .To<FitnessActionDto>()
                    .ToListAsync();
            }
        }

        /// <summary>
        /// The get by fitness program.
        /// </summary>
        /// <param name="fitnessProgramId">
        /// The fitness program id.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        public async Task<List<FitnessActionDto>> GetByFitnessProgram(int fitnessProgramId)
        {
            using (var context = this.contextFactory.Create())
            {
                return
                    await
                    context.FitnessProgramActionses.Where(f => f.FitnessProgramId == fitnessProgramId)
                        .Join(context.FitnessActions, j => j.FitnessActionId, f => f.Id, (program, action) => action)
                        .Project()
                        .To<FitnessActionDto>()
                        .ToListAsync();
            }
        }
    }
}