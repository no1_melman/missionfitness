﻿namespace MissionFitness.Gui.App_Start.Bundles
{
    using System.Web.Optimization;

    /// <summary>
    /// Angular bundles class
    /// </summary>
    public static class Angular
    {
        /// <summary>
        /// Angular scripts.
        /// </summary>
        /// <returns>
        /// The <see cref="Bundle"/>.
        /// </returns>
        public static Bundle Scripts()
        {
            return new ScriptBundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/angular-ui-router.js",
                      "~/Scripts/angular-resource.js",
                      "~/Scripts/angular-local-storage.js");
        }
    }
}